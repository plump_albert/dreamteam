@extends("app")
@section("container-class", "items-center px-6 py-12 gap-6")
@section("content")
<img class="mx-4 w-full" src="{{ url('images/logo.png') }}" alt="logo.png" />
<h1 class="text-3xl font-black font-serif uppercase">#DreamTeam</h1>
<p class="font-sans font-medium text-justify text-xl">
	Будь в курсе последних интересных событий Discord сервера!
</p>
<p class="text-base font-sans font-regular text-justify">
	Хватит листать чаты в поисках интересующей информации среди груды
	сообщений от
	<a class="text-emerald-600 bg-emerald-100 p-1 rounded" href="https://vk.com/kki98">@керила</a>
	о вове! Не пропусти ни одного важного для тебя
	сообщения среди кучи всякого говна, которым захломляют чаты.
</p>
<a class="w-full py-2 bg-emerald-500 font-medium text-white uppercase rounded text-center"
	href="https://discord.com/api/oauth2/authorize?client_id=956850410881183744&redirect_uri=http%3A%2F%2Flocalhost%2Fcallback%2Fdiscord&response_type=code&scope=identify">
	Войти через Discord
</a>
@endsection
