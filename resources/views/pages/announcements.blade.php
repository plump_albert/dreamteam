@extends("app")
@section("container-class","px-4 pt-8 items-center")
@section("content")
<h1 class="w-full box-border text-2xl font-sans text-black px-8">Объявления</h1>
<div class="flex flex-col w-full gap-1 mt-8">
	<div class="flex flex-col w-full text-base text-black py-3 pr-2 gap-3">
		<div class="flex flex-row flex-1 items-center justify-start">
			<span class="text-xl leading-6 w-6 mr-2 fab fa-discord"></span>
			<span class="text-[#5865f2] font-bold">Plump_Albert #9145</span>
			<span class="ml-auto text-xs text-gray-500">#movie-announce</span>
		</div>
		<div class="flex flex-col flex-1 ml-8 gap-3">
			<p class="line-clamp-3 text-justify">
				Good Night, and Good Luck (2005)<br />
				When Senator Joseph McCarthy begins his foolhardy campaign to root out Communists in America, CBS News
				impresario Edward R. Murrow (David Strathairn) dedicates himself to exposing the atrocities being
				committed
				by
				McCarthy's Senate "investigation." Murrow is supported by a news team that includes long-time friend and
				producer Fred Friendly (George Clooney). The CBS team does its best to point out the senator's lies and
				excesses, despite pressure from CBS' corporate sponsors to desist.
			</p>
			<div class="flex flex-row flex-1 items-center justify-between">
				<div class="flex flex-row gap-2 text-xs">
					<div class="flex flex-row gap-[0.5em] px-[0.5em] py-[0.25em] bg-gray-100 rounded">
						<span class="emoji leading-none">👍</span>
						<span class="text-gray-600">3</span>
					</div>
					<div class="flex flex-row gap-[0.5em] px-[0.5em] py-[0.25em] bg-primary-100 rounded">
						<span class="emoji leading-none">🎉</span>
						<span class="text-black">6</span>
					</div>
				</div>
				<span class="text-xs text-gray-500"> 01 марта 2022 </span>
			</div>
		</div>
	</div>

	<hr>

	<div class="flex flex-col w-full text-base text-black py-3 pr-2 gap-3">
		<div class="flex flex-row flex-1 items-center justify-start">
			<span class="text-xl leading-6 w-6 mr-2 fab fa-discord"></span>
			<span class="text-[#5865f2] font-bold">Plump_Albert #9145</span>
			<span class="ml-auto text-xs text-gray-500">#movie-announce</span>
		</div>
		<div class="flex flex-col flex-1 ml-8 gap-3">
			<p class="line-clamp-3 text-justify">
				киношечка завтра в 18:00. Будем смотреть порнуху
			</p>
			<div class="flex flex-row flex-1 items-center justify-between">
				<div class="flex flex-row gap-2 text-xs">
					<div class="flex flex-row gap-[0.5em] px-[0.5em] py-[0.25em] bg-gray-100 rounded">
						<span class="emoji leading-none">🤮</span>
						<span class="text-gray-600">3</span>
					</div>
					<div class="flex flex-row gap-[0.5em] px-[0.5em] py-[0.25em] bg-gray-100 rounded">
						<span class="emoji leading-none">🍆</span>
						<span class="text-black">2</span>
					</div>
				</div>
				<span class="text-xs text-gray-500"> 01 марта 2022 </span>
			</div>
		</div>
	</div>
</div>
@endsection
