@extends("app")
@section("container-class", "items-center px-6 my-auto gap-6 flex-initial")
<?php $showNavbar = false; ?>
@section("content")
	<span class="fa-solid fa-check text-[128px] text-emerald-500"></span>
	<h1 class="font-sans font-medium text-2xl text-center">Регистрация завершена</h1>
	<p class="font-sans font-base text-center">
		Пожалуйста, подождите, через несколько секунд Вас перенаправит на
		главную страницу. В данный момент мы крадем Вашу личную информацию.
	</p>
<script>setTimeout(function () {window.location.replace("/user")}, 3000)</script>
@endsection
