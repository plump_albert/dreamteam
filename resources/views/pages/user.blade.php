@extends("app")
@section("container-class","px-6 pt-16 items-center")
@section("content")
<img class="w-[128px] y-[128px] rounded-full border border-solid border-gray-200" src="{{ $avatar ? "
	https://cdn.discordapp.com/avatars/$user_id/$avatar.png" : url('images/logo.png') }}" alt="user.logo">
<h1 class="mt-6 text-2xl uppercase">{{ $user_name }}</h1>
<div class="flex flex-col w-full mt-8">
	@foreach ($connections as $connection)
	<div class="flex flex-1 flex-row items-center py-3">
		<span class="fa-brands text-xl mr-3 {{ $connection->provider->icon }}"></span>
		<span class="text-base capitalize">{{ $connection->provider->name }}</span>
		<span class="text-base text-gray-500 ml-auto">
			{{ $connection->username }}
		</span>
	</div>
	@endforeach
	@foreach ($apps as $app)
	<div class="flex flex-1 flex-row items-center py-3">
		<span class="fa-brands text-xl mr-3 {{ $app->icon }}"></span>
		<span class="text-base capitalize">{{ $app->name }}</span>
		<a class="text-base text-gray-500 active:bg-gray-200 hover:bg-gray-200 rounded-md p-2 ml-auto mr-[-0.5rem] uppercase"
			href="">
			Подключить
		</a>
	</div>
	@endforeach
</div>
<p class="flex mt-8 w-full text-gray-500 text-center">
	В будущем будут доступны другие платформы! Следите за обновлениями!
</p>
@endsection
