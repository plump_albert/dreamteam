<div class="flex w-full shadow-up-2 {{ $attributes }}">
	<a class="flex flex-1 py-3 justify-center
		@if(Request::is("user")) bg-primary-100 text-primary-600 @else bg-white text-gray-400 @endif" href="/user">
		<span class="fa-solid fa-user text-base text-current"></span>
	</a>

	<a class="flex flex-1 py-3 justify-center
		@if(Request::is("announcements")) bg-primary-100 text-primary-600 @else bg-white text-gray-400 @endif"
		href="/announcements">
		<span class="fa-solid fa-home text-base text-current"></span>
	</a>

	<a class="flex flex-1 py-3 justify-center
		@if(Request::is("construction")) bg-primary-100 text-primary-600 @else bg-white text-gray-400 @endif" href="/dev">
		<span class="fa-solid fa-person-digging text-base text-current"></span>
	</a>
</div>
