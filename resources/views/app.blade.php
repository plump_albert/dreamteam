<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="{{ url('css/fonts.css') }}" rel="stylesheet">
	<link rel="stylesheet" href="{{ URL('css/brands.min.css') }}">
	<link rel="stylesheet" href="{{ URL('css/fontawesome.min.css') }}">
	<link rel="stylesheet" href="{{ URL('css/regular.min.css') }}">
	<link rel="stylesheet" href="{{ URL('css/solid.min.css') }}">
	<link rel="stylesheet" href="{{ url('css/app.css') }}">
	<title>@yield('title', config('app.name'))</title>
</head>

<body>
	<div class="flex flex-col flex-1 w-full overflow-x-hidden overflow-y-auto @yield('container-class', '')">
		@yield('content')
	</div>
	<?php
	if (!isset($showNavbar)) {
		$showNavbar = true;
	}
	?>
	@if (!Request::is("/") && $showNavbar)
	<x-nav-bar class="mt-auto" page="{{ Route::current()->getName() }}" />
	@endif
</body>

</html>
