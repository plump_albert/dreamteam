<?php
/*
 * This document has been generated with
 * https://mlocati.github.io/php-cs-fixer-configurator/#version:3.8.0|configurator
 * you can change this configuration by importing this file.
 */
$config = new PhpCsFixer\Config();
return $config
	->setRiskyAllowed(true)
	->setIndent("\t")
	->setRules([
		'@PSR12' => true,
		// Replace multiple nested calls of `dirname` by only one call with second `$level` parameter. Requires PHP >= 7.0.
		'combine_nested_dirname' => true,
	])
	->setFinder(
		PhpCsFixer\Finder::create()
		->exclude('vendor')
		->in(__DIR__)
	)
;
