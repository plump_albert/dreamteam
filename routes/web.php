<?php

use App\Http\Controllers\Callback;
use App\Http\Controllers\Landing;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [Landing::class, 'index']);
Route::get('/callback/{provider}', [Callback::class, 'show']);
Route::get('/user', [UserController::class, 'show']);
Route::get('/announcements', function () {
	return view('pages.announcements');
});
