const colors = require("tailwindcss/colors");
module.exports = {
	content: [
		"./resources/**/*.blade.php",
		"./resources/**/*.jsx",
		"./resources/**/*.tsx",
	],
	theme: {
		colors: {
			...colors,
			primary: {
				50: "#ECF4F0",
				100: "#CFE7DB",
				400: "#1BC072",
				600: "#0C8D50",
			},
		},
		fontFamily: {
			sans: ["Lato", "ui-sans-serif", "system-ui"],
			serif: ['"Inknut Antiqua"', "ui-serif", "system-ui"],
		},
		extend: {
			boxShadow: {
				"up-2":
					"0px -1px 3px rgba(0, 0, 0, 0.16), 0px -2px 4px 2px rgba(0, 0, 0, 0.08)",
			},
		},
	},
	plugins: [require("@tailwindcss/line-clamp")],
};
