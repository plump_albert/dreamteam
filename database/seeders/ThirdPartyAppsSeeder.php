<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ThirdPartyAppsSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$table = DB::table('third_party_apps');
		if ($table->count() != 0) {
			return;
		}
		$table->insert([
			[
				'name' => 'Discord',
				'icon' => 'fa-discord'
			], [
				'name' => 'Telegram',
				'icon' => 'fa-telegram'
			]
		]);
	}
}
