# DREAMTEAM

## Gibberish about this project

# Authorization flow

## Sign up

- [ ] Redirect new users to Discord for registration;
- [ ] Obtain `access_token` and `refresh_token` using user's code;
- [ ] Get user's email from Discord and create user's record in APP;
- [ ] Create record about user's connection to Discord (`access_token` and
      `refresh_token`);
- [ ] Issue APP `access_token` and `refresh_token` to authenticate user's
      actions;
- [ ] Be happy :blush:.

## Sequential logins

- [ ] User asks APP to perform some task and sends APP's `access_token` via
      cookie;
- [ ] APP checks that `access_token` is valid and not expired;
- [ ] APP performs task.

## Expired token

- [ ] User asks APP to perform some task and sends APP's `access_token` via
      cookie;
- [ ] APP checks that `access_token` is valid and not expired;
- [ ] Access token is expired, so APP redirects USER to `login` route.
- [ ] USER goes to `login` route and sends APP's `access_token` and
      `refresh_token` via cookies;
- [ ] APP checks if `refresh_token` is valid;
- [ ] APP reissues new pair of `access_token` and `refresh_token` and redirects
      USER back to previous resource;
- [ ] USER re-asks APP to perform some task from step 1. with newly created
      `access_token`.
