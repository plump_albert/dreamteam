<?php

namespace App\OAuth\Providers;

use App\OAuth\Provider;

class DiscordProvider extends Provider
{
	public function __construct($redirect_uri = null)
	{
		if (!isset($redirect_uri)) {
			$redirect_uri = env("APP_URL") . "/callback/discord";
		}
		$this->API_ENDPOINT = "https://discord.com/api/v8";
		$this->API_REDIRECT = $redirect_uri;
		$this->CLIENT_ID = env("DISCORD_CLIENT_ID");
		$this->CLIENT_SECRET = env("DISCORD_CLIENT_SECRET");
	}

	public function authenticate($user_code)
	{
		return $this->postRequest(
			$this->API_ENDPOINT . '/oauth2/token',
			[
				'redirect_uri' => $this->API_REDIRECT,
				'client_id' => $this->CLIENT_ID,
				'client_secret' => $this->CLIENT_SECRET,
				'grant_type' => 'authorization_code',
				'code' => $user_code,
			],
			['Content-Type' => 'application/x-www-form-urlencoded']
		);
	}

	public function getUserInfo($accessToken)
	{
		return $this->getRequest($this->API_ENDPOINT . "/users/@me", [], [
			'Authorization' => 'Bearer ' . $accessToken
		]);
	}

	public function refreshToken($refreshToken)
	{
		return $this->postRequest(
			$this->API_ENDPOINT . '/oauth2/token',
			[
				'client_id' => $this->CLIENT_ID,
				'client_secret' => $this->CLIENT_SECRET,
				'grant_type' => 'refresh_token',
				'refresh_token' => $refreshToken,
			],
			['Content-Type' => 'application/x-www-form-urlencoded']
		);
	}
}
