<?php

namespace App\OAuth;

abstract class Provider
{
	protected $API_ENDPOINT;
	protected $CLIENT_ID;
	protected $CLIENT_SECRET;
	protected $API_REDIRECT;

	/**
	 * Performs GET request to `$uri` with `$query` and `$headers`
	 *
	 * @param string $uri URI of resource
	 * @param array $query Key-value array of query
	 * @param array $headers Key-value pairs of request headers
	 *
	 * @return array Response from the requested URI
	 */
	protected static function getRequest($uri, $query = [], $headers = [])
	{
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $uri . "?" . http_build_query($query));
		$headersList = [];
		foreach ($headers as $key => $val) {
			$headersList[] = "$key: $val";
		}
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headersList);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		$json = curl_exec($curl);
		curl_close($curl);
		return json_decode($json, true);
	}

	/**
	 * Performs POST request to `$uri` with `$data` and `$headers`
	 *
	 * @param string $uri URI of resource
	 * @param array $data Key-value array with data for POST request
	 * @param array $headers Key-value pairs of request headers
	 *
	 * @return array Response from the requested URI
	 */
	protected static function postRequest($uri, $data, $headers = [])
	{
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $uri);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
		$headersList = [];
		foreach ($headers as $key => $val) {
			$headersList[] = "$key: $val";
		}
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headersList);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		$json = curl_exec($curl);
		curl_close($curl);
		return json_decode($json, true);
	}

	/**
	 * Get pair access_token/refresh_token from provider
	 *
	 * @param string $user_code User code for authentication
	 *
	 * @return array Associative array containing access token, refresh token and expiration
	 * time
	 */
	abstract public function authenticate($user_code);


	/**
	 * Get information about user via `$accessToken`
	 *
	 * @param string $accessToken Access token of user
	 *
	 * @return Information about the user
	 */
	abstract public function getUserInfo($accessToken);

	/**
	 * Refreshes access token using refresh token
	 *
	 * @param string $refreshToken Refresh token to use
	 *
	 * @return OAuthData New pair access/refresh tokens and expiration time
	 */
	abstract public function refreshToken($refreshToken);
}
