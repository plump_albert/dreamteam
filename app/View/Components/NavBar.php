<?php

namespace App\View\Components;

use Illuminate\View\Component;

class NavBar extends Component
{
	/**
	 * Current page
	 * @var string
	 */
	public $page;
	/**
	 * Create a new component instance.
	 *
	 * @param string $page
	 * @return void
	 */
	public function __construct($page)
	{
		$this->page = $page;
	}

	/**
	 * Get the view / contents that represent the component.
	 *
	 * @return \Illuminate\Contracts\View\View|\Closure|string
	 */
	public function render()
	{
		return view('components.nav-bar');
	}
}
