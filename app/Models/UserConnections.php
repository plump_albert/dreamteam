<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserConnections extends Model
{
	use HasFactory;
	public $timestamps = false;

	/**
	 * Get `ThirdPartyApp` for current connection
	 */
	public function provider()
	{
		return $this->belongsTo(ThirdPartyApps::class, 'provider_id');
	}

	/**
	 * Get `User` for current connection
	 */
	public function user()
	{
		return $this->belongsTo(User::class, 'user_id');
	}
}
