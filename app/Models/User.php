<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
	use HasApiTokens;
	use HasFactory;
	use Notifiable;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array<int, string>
	 */
	protected $fillable = [
		'email',
		'access_token',
		'refresh_token',
		'expires_in',
	];

	/**
	 * Get all connected apps for current user
	 */
	public function connectedApps()
	{
		return $this->belongsToMany(
			ThirdPartyApps::class,
			'user_connections',
			'user_id',
			'provider_id'
		);
	}
}
