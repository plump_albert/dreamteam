<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ThirdPartyApps extends Model
{
	use HasFactory;
	public $timestamps = false;

	/**
	 * Get all `UserConnections` associated with this application
	 */
	public function connections()
	{
		return $this->hasMany(UserConnections::class, 'provider_id');
	}
}
