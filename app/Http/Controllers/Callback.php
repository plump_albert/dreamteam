<?php

namespace App\Http\Controllers;

use App\Models\ThirdPartyApps;
use App\Models\User;
use App\Models\UserConnections;
use App\OAuth\Providers\DiscordProvider;
use Illuminate\Http\Request;

class Callback extends Controller
{
	public function show(Request $request, $providerName)
	{
		$request->validate([
			'code' => 'required'
		]);
		$oauthProvider = null;
		switch ($providerName) {
			case 'discord': {
					$redirect = env('APP_DEBUG')
						? 'http://localhost/callback/discord'
						: env('APP_URL') . '/callback/discord';
					$oauthProvider = new DiscordProvider($redirect);
					break;
				}
			default: {
					return response(json_encode([
						'error' => true,
						'message' => 'Provider is not supported'
					]), 501, ['Content-Type' => 'application/json']);
				}
		}
		# Get user's `access_token` and `refresh_token`
		$tokens = $oauthProvider->authenticate($request->code);
		if (array_key_exists('error', $tokens)) {
			return response(
				json_encode($tokens),
				401,
				['Content-Type' => 'application/json']
			);
		}
		# Get information about the user
		$userInfo = $oauthProvider->getUserInfo($tokens["access_token"]);
		$request->session()->put('discord_data', $userInfo);
		# If provider is Discord - register new user
		if ($providerName === "discord") {
			$user = User::firstWhere(['email' => $userInfo['email']]);
			if (!$user) {
				$user = User::create(['email' => $userInfo['email']]);
			}
			$request->session()->put('user_id', $user->id);
			$request->session()->put('user_name', $userInfo["username"]);
		}
		$userId = $request->session()->get('user_id');
		if (!isset($userId)) {
			return redirect()->intended();
		}
		$providerModel = ThirdPartyApps::select('id')
			->where('name', 'ilike', $providerName)
			->first();
		$connection = UserConnections::where([
			'user_id' => $userId,
			'provider_id' => $providerModel->id
		])->first();
		if (!isset($connection)) {
			echo "NONSENSE!";
			$connection = new UserConnections();
			$connection->user_id = $userId;
			$connection->provider_id = $providerModel->id;
			$connection->updated_at = new \DateTime();
		} elseif ($connection->access_token !== $tokens["access_token"]) {
			$connection->updated_at = new \DateTime();
		}
		$connection->access_token = $tokens["access_token"];
		$connection->refresh_token = $tokens["refresh_token"];
		$connection->expires_in = $tokens["expires_in"];
		switch ($providerName) {
			case 'discord':
				$connection->username = $userInfo['username'] . ' #' . $userInfo['discriminator'];
				break;
			default:
				$connection->username = $userInfo['email'];
				break;
		}
		$connection->save();
		return view('pages.callback');
	}
}
