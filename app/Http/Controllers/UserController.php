<?php

namespace App\Http\Controllers;

use App\Models\ThirdPartyApps;
use App\Models\UserConnections;
use App\OAuth\Providers\DiscordProvider;
use Illuminate\Http\Request;

class UserController extends Controller
{
	public function show(Request $request)
	{
		$userId = $request->session()->get('user_id');
		if (!isset($userId)) {
			return redirect('/');
		}
		$connections = UserConnections::with('provider')
			->where(['user_id' => $userId])
			->get();
		$apps = ThirdPartyApps::all()->except(
			$connections->pluck('provider_id')->all()
		);
		$discordData = $request->session()->get('discord_data');
		if (!isset($discordData)) {
			echo 'lol';
			$discordConnection = $connections->filter(function ($item) {
				return strcasecmp($item->name, 'discord');
			})->first();
			# Make call to Discord API
			$provider = new DiscordProvider();
			$discordData = $provider->getUserInfo($discordConnection->access_token);
		}
		return view('pages.user', [
			'avatar' => $discordData['avatar'],
			'user_id' => $discordData['id'],
			'user_name' => $request->session()->get('user_name'),
			'connections' => $connections,
			'apps' => $apps
		]);
	}
}
